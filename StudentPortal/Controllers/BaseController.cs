﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace StudentPortal.Controllers
{
    public class BaseController : Controller
    {
        public BaseController ()
        {
            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                ViewBag.Sid = System.Web.HttpContext.Current.Session["Sid"];
                ViewBag.Studentname = System.Web.HttpContext.Current.Session["Studentname"];
            }
            else
            {
                System.Web.HttpContext.Current.Response.Redirect("/Login/Login");
            }

        }


        
    }
}