﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentLibrary.Models;
using System.Security.Cryptography;
using StudentLibrary.classes;

namespace StudentPortal.Controllers
{
    public class LoginController : Controller
    {
        StudentRegistrationEntities ab = new StudentRegistrationEntities();
        clsCommon objCommon = new clsCommon();
        clsStudent objStudent = new clsStudent();
      
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(StdRegistration abc)
        {
                abc.Password = objCommon.GenerateMD5(abc.Password);
            var result = ab.StdRegistrations.Where(x => x.Email == abc.Email && x.Password==abc.Password).FirstOrDefault();
             if(result!=null)
            {
                Session["Sid"] = result.Sid;
              
                Session["Studentname"] = result.Firstname+result.Lastname;
                return RedirectToAction("Index","Home");
            }
             else
            {
                return RedirectToAction("Register","Student");
            }
            
        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Register(StdRegistration model)
        {

            model.Password = objCommon.GenerateMD5(model.Password);
            objStudent.SaveStudent(model);

           
            {
                    return RedirectToAction("Login");
                }
            
        }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            System.Web.HttpContext.Current.Response.Redirect("/Login/Login");
            return View();
        }

    }
}