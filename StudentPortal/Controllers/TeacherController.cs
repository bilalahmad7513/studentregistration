﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentLibrary.Models;
using StudentLibrary.classes;

namespace StudentPortal.Controllers
{
    public class TeacherController : BaseController
    {
        StudentRegistrationEntities ab = new StudentRegistrationEntities();
        clsCommon objCommon = new clsCommon();
        clsTeacher objTeacher = new clsTeacher();


        public ActionResult Teachers()
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            var item = ab.TeacherRegistrations.ToList();
            ViewBag.List = item;
            return View();
         
        }

        [HttpGet]
        public ActionResult AddTeacher(int id=0 )
        {
            var item = ab.TeacherRegistrations.FirstOrDefault(x => x.Tid == id);
            ViewBag.item = item;
            return View("AddTeacher", item);

           
        }
        [HttpPost]
        public ActionResult AddTeacher(TeacherRegistration model)
        {
            if (model.Tid > 0)
            {
                model.Password = objCommon.GenerateMD5(model.Password);
                objTeacher.EditTeacher(model);
                return RedirectToAction("Teachers");
            }
            else
            {

                model.Password = objCommon.GenerateMD5(model.Password);
                objTeacher.AddTeacher(model);
                return RedirectToAction("Teachers");
            }
        }

        public ActionResult Delete(int id)
        {

            var item = ab.TeacherRegistrations.Where(x => x.Tid == id).First();
            ab.TeacherRegistrations.Remove(item);
            ab.SaveChanges();
            return RedirectToAction("Teachers");
        }
        public ActionResult ProfileViewTeacher(int id)
        {

            var item = ab.TeacherRegistrations.Where(x => x.Tid == id).FirstOrDefault();
            ViewBag.item = item;
            var item1 = ab.v_TeacherAssign.Where(x => x.Tid == id).ToList();
            ViewBag.item1 = item1;
            ViewBag.TeacherId = id;
            return View();
        }
        [HttpGet]
        public ActionResult TeacherClass(int TeacherClassId =0, int TeacherId=0  )
        {
            var item = ab.TeacherAssigns.FirstOrDefault(x => x.TeacherClassId == TeacherClassId);
            ViewBag.AllClasses = ab.Classes.Select(x => new SelectListItem { Text = x.ClassName.ToString(), Value = x.ClassId.ToString() }).ToList();
            ViewBag.TeacherId = TeacherId;
            if (TeacherClassId>0)
            {
                var item1 = ab.TeacherAssigns.FirstOrDefault(x => x.TeacherClassId == TeacherClassId);
                ViewBag.TeacherClassId = TeacherClassId;
                return View(item1);
            }
           
          
            
            return View("TeacherClass",item);
        }
        [HttpPost]
        public ActionResult TeacherClass(TeacherAssign model)
        {
            if (model.TeacherClassId > 0)
            {
                var item = ab.TeacherAssigns.FirstOrDefault(x => x.Tid == model.Tid);
                item.ClassId = model.ClassId;
                item.Tid = model.Tid;
                ab.SaveChanges();
            }
            else
            {
                TeacherAssign obj = new TeacherAssign();
                obj.Tid = model.Tid;
                obj.ClassId = model.ClassId;
                ab.TeacherAssigns.Add(obj);
                ab.SaveChanges();

                //ID = ab.StudentClasses.Select(x => x.StudentClassId).Max();
            }
            return RedirectToAction("ProfileViewTeacher", new { id = model.Tid });
        }
        public ActionResult DeleteTeacherClass(int id)
        {
            var item = ab.TeacherAssigns.Where(x => x.TeacherClassId == id).First();
            ab.TeacherAssigns.Remove(item);
            ab.SaveChanges();
            return RedirectToAction("Teachers");
        }
    }
}