﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentPortal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}