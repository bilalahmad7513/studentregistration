﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentLibrary.Models;
using System.Text;
using StudentLibrary.classes;

namespace StudentPortal.Controllers
{
    public class StudentController : BaseController
    {
        StudentRegistrationEntities ab = new StudentRegistrationEntities();
        clsCommon objCommon = new clsCommon();
        clsStudent objStudent = new clsStudent();


        [HttpGet]

        public ActionResult Edit(int id)
        {
            var item = ab.StdRegistrations.FirstOrDefault(x => x.Sid == id);
            return View("Edit", item);
        }
        [HttpPost]
        public ActionResult Edit(StdRegistration model)
        {
            model.Password = objCommon.GenerateMD5(model.Password);
             if (model.Sid > 0)
             {
                objStudent.EditForm(model);
                return RedirectToAction("StudentRecord");
             }
            return View();
             }
                
        [HttpGet]
        public ActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddStudent(StdRegistration model)

        {
            int studentID = 0;
            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
             objStudent.SaveStudent(model);    
            {
                return RedirectToAction("StudentRecord");
            }
        }
        public ActionResult StudentRecord()
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }

            var item = ab.StdRegistrations.ToList();
            ViewBag.List = item;
            return View();
        }
        public ActionResult Delete(int id)
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            objStudent.DeleteRecord(id);
            return RedirectToAction("StudentRecord");
        }
        public ActionResult ProfileView(int id)
        {
            
            var item = ab.StdRegistrations.Where(x => x.Sid == id).FirstOrDefault();
            ViewBag.item = item;
            var item1 = ab.v_StudentClass.Where(x => x.StudentId == id).ToList();  
            ViewBag.item1 = item1;
            ViewBag.StudentID = id;
            return View();
        }
        public ActionResult Classes()
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            var item = ab.Classes.ToList();
            ViewBag.item = item;
            return View();
        }
        [HttpGet]
        public ActionResult AddClass()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddClass(Class model)
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        
            return RedirectToAction("Classes");
        }
        public ActionResult Delete1(int id)
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }

            var item = ab.Classes.Where(x => x.ClassId == id).First();
            ab.Classes.Remove(item);
            ab.SaveChanges();
            return RedirectToAction("Classes");
        }
        [HttpGet]
        public ActionResult StudentClass(int StudentID)
        {
            ViewBag.AllClasses = ab.Classes.Select(x => new SelectListItem { Text = x.ClassName.ToString(), Value = x.ClassId.ToString() }).ToList();
            ViewBag.StudentID = StudentID;
            return View();
        }
        [HttpPost]
        public ActionResult StudentClass(StudentClass model)
        {
            //int ID = 0;
            objStudent.CountClass(model);
            //ID = ab.StudentClasses.Select(x => x.StudentClassId).Max();

            return RedirectToAction("ProfileView", new {id = model.StudentId });
        }
        [HttpGet]
        public ActionResult Edit1(int id)
        {
            var item = ab.Classes.FirstOrDefault(x => x.ClassId == id);
            return View("Edit1", item);

        
        }
        [HttpPost]
        public ActionResult Edit1(Class model)
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            if (model.ClassId > 0)
            {
                var item = ab.Classes.FirstOrDefault(x => x.ClassId == model.ClassId);
                item.ClassName = model.ClassName;
                ab.SaveChanges();
                return RedirectToAction("Classes");
            }
            return View();
        }
        public ActionResult Delete2(int id)
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            var item = ab.StudentClasses.Where(x => x.StudentClassId == id).First();
            ab.StudentClasses.Remove(item);
            ab.SaveChanges();
            return RedirectToAction("StudentRecord");
        }
        [HttpGet]
        public ActionResult Edit2(int id, int StudentId)
        {
            var item = ab.StudentClasses.FirstOrDefault(x => x.StudentClassId == id);
            ViewBag.AllClasses = ab.Classes.Select(x => new SelectListItem { Text = x.ClassName.ToString(), Value = x.ClassId.ToString() }).ToList();
            ViewBag.StudentId = StudentId;

            return View("Edit2", item);


        }
        [HttpPost]
        public ActionResult Edit2(StudentClass model)
        {
            int studentID = 0;

            if (!String.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["Sid"])))
            {
                studentID = Convert.ToInt32(System.Web.HttpContext.Current.Session["Sid"]);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            objStudent.ClassEdit(model);
            if (model.StudentId > 0)
            {             
                return RedirectToAction("StudentRecord");
            }
            return View();
        }
    }
}