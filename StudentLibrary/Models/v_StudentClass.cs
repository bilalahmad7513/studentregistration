//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StudentLibrary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_StudentClass
    {
        public int StudentClassId { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ClassName { get; set; }
    }
}
