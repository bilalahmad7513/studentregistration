﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentLibrary.Models;

namespace StudentLibrary.classes
{
    public class clsTeacher
    {
        StudentRegistrationEntities ab = new StudentRegistrationEntities();
        public void AddTeacher(TeacherRegistration model)
        {
            TeacherRegistration obj = new TeacherRegistration();
            obj.TeacherName = model.TeacherName;
            obj.TeacherCnic = model.TeacherCnic;
            obj.Address = model.Address;
            obj.Email = model.Email;
            obj.Password = model.Password;
            ab.TeacherRegistrations.Add(obj);  
            ab.SaveChanges();
        }

        public void EditTeacher(TeacherRegistration model)
        {
            var obj = ab.TeacherRegistrations.FirstOrDefault(x => x.Tid == model.Tid);
            obj.TeacherName = model.TeacherName;
            obj.TeacherCnic = model.TeacherCnic;
            obj.Address = model.Address;
            obj.Email = model.Email;
            obj.Password = model.Password;
            ab.SaveChanges();
        }
    }
}
