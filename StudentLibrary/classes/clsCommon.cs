﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StudentLibrary.classes
{
    public class clsCommon
    {
        public string GenerateMD5(string Password)        {            return string.Join("", MD5.Create().ComputeHash(System.Text.Encoding.ASCII.GetBytes(Password)).Select(s => s.ToString("x2")));        }
        
    }
}
