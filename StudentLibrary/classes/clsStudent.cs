﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentLibrary.Models;

namespace StudentLibrary.classes
{

    public class clsStudent
    {
        StudentRegistrationEntities ab = new StudentRegistrationEntities();

        public object ViewBag { get; private set; }

        public void SaveStudent(StdRegistration model)
        {
            StdRegistration obj = new StdRegistration();
            obj.Firstname = model.Firstname;
            obj.Lastname = model.Lastname;
            obj.Fathername = model.Fathername;
            obj.StudentCnic = model.StudentCnic;
            obj.Address = model.Address;
            obj.Email = model.Email;
            obj.Password = model.Password;
            ab.StdRegistrations.Add(obj);
            ab.SaveChanges();
        }

        public void EditForm(StdRegistration model)
        {
           

                var item = ab.StdRegistrations.FirstOrDefault(x => x.Sid == model.Sid);
                item.Firstname = model.Firstname;
                item.Lastname = model.Lastname;
                item.Fathername = model.Fathername;
                item.StudentCnic = model.StudentCnic;
                item.Address = model.Address;
                item.Email = model.Email;
                if (model.Password != null)
                {
                    item.Password = model.Password;
                }

                ab.SaveChanges();
            }

        public void DeleteRecord(int id)
        {
            var item = ab.StdRegistrations.Where(x => x.Sid == id).First();
            ab.StdRegistrations.Remove(item);
            ab.SaveChanges();
        }

        public void ClassesAdd(Class model)
        {

            Class obj = new Class();
            obj.ClassName = model.ClassName;
            ab.Classes.Add(obj);
            ab.SaveChanges();
        }
        public void CountClass(StudentClass model)
        {
            StudentClass obj = new StudentClass();
            int count = 0;
            count = ab.StudentClasses.Where(x => x.StudentId == model.StudentId).Count();
            if (count > 0)
            {

            }
            else
            {
                obj.StudentId = model.StudentId;
                obj.ClassId = model.ClassId;
                ab.StudentClasses.Add(obj);
                ab.SaveChanges();
            }
        }

        public void ClassEdit(StudentClass model)
        {

            var item = ab.StudentClasses.FirstOrDefault(x => x.StudentId == model.StudentId);
            item.ClassId = model.ClassId;
            item.StudentId = model.StudentId;
            ab.SaveChanges();
        }

      
    }
}
